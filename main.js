'use strict';



const {app,BrowserWindow} = require('electron');
// require('electron-reload')(__dirname);


function createWindow() {
  let win = new BrowserWindow({
    width: 1100,
    height: 600,
    webPreferences: {
      nodeIntegration: true
    }
  })
  win.loadFile('index.html');
}

// Menu.setApplicationMenu(null);
app.on('ready', createWindow)