const firebase = require("firebase");
const date = require('date-and-time');
const storage = require('electron-json-storage');

require("firebase/firestore");

firebase.initializeApp({
    apiKey: "AIzaSyDRAurj89rKfwwBMoRjQ5JfzRKUDM9ax-I",
    authDomain: "desktop-app-8b038.firebaseapp.com",
    projectId: "desktop-app-8b038",
});

var db = firebase.firestore();

// Configuration Data to local storage
let a_data;
let admin_status;

let configData;
var depAmount = 0;
var monthlyfee = 0;

var billStartMonth = "";
var memDets;
// var memberDep = 0;
var queryString = "";
var bill_info;


storage.get('admin_stat', function (error, status) {
    if (error) {
        console.log("Error: " + error)
    } else {
        a_data = status;
        admin_status = parseInt(a_data["admin"]);
        console.log(admin_status)

        if (Boolean(admin_status) == true) {
            // alert('entered ' + Boolean(admin_status))
            var arrayOfElements = document.getElementsByClassName('admin_feat');
            var lengthOfArray = arrayOfElements.length;

            for (var i = 0; i < lengthOfArray; i++) {
                arrayOfElements[i].style.display = 'block';
            }
        }
    }
});

storage.get('config', function (error, tdata) {
    if (error) {
        console.log("Error: " + error)
    } else {
        // console.log(tdata);
        configData = tdata;
        depAmount = configData['DepAmt'];
        lateFee = parseInt(configData['MonLateFee']);

    }
});

// Ends here


function searchNumber() {
    document.getElementById("load-spinner").style.display = "block";
    document.getElementById("noData").style.display = "none";
    document.getElementById("memberDets").style.display = "none";

    toSearch = document.getElementById("search_input").value;
    db.collection("Member-Details").where("memberNumber", "==", toSearch).get()
        .then(function (querySnapshot) {
            document.getElementById("load-spinner").style.display = "none";

            if (querySnapshot.empty == true) {
                // alert("No member found.");
                document.getElementById("noData").style.display = "block";
                document.getElementById("memberDets").style.display = "none";

            } else {
                querySnapshot.forEach(function (doc) {
                    document.getElementById("noData").style.display = "none";
                    document.getElementById("memberDets").style.display = "block";
                    memDets = doc.data();
                    console.log(memDets);


                    document.getElementById('plot').innerText = doc.data().plot;
                    document.getElementById('type').innerText = doc.data().plotType;
                    document.getElementById('memN').innerText = doc.data().memberNumber;
                    document.getElementById('name').innerText = doc.data().fullName;
                    document.getElementById('phone').innerText = doc.data().phone;
                    document.getElementById('dep').innerText = doc.data().deposit;

                    if (doc.data().status == "Connected") {
                        // alert("Entered connected")
                        document.getElementById("dis_btn").checked = false;
                    } else if (doc.data().status == "Disconnected") {
                        // alert("Entered disconnected")
                        document.getElementById("dis_btn").checked = true;
                    }
                    // document.getElementById('stat').innerText = doc.data().status;
                    // + '<label class="switch"><input type="checkbox"><span class="slider round"></span></label>'';


                });
            }
        }).catch(function (error) {
            alert("An error has occured. Please retry.");
        })
}

function disconnect() {
    var mem = memDets["memberNumber"];
    // var sel = document.getElementById('upd-selectedPlotType')
    // var selValue = sel.options[sel.selectedIndex].text;
    // console.log(memDets["memberNumber"] + selValue + document.getElementById("upd-phone").value)


    if (document.getElementById("dis_btn").checked) {
        var res = confirm("Are you sure you want to disconnect this member?")
        if (res) {
            document.getElementById("dis_btn").checked = true;


            db.collection("Member-Details").where("memberNumber", "==", mem).get()
                .then(function (querySnapshot) {
                    querySnapshot.forEach(function (doc) {
                        db.collection("Member-Details").doc(doc.id).update({
                                status: "Disconnected",
                            })
                            .then(function () {
                                alert("Member disconnected successfully!");
                            })
                            .catch(function (error) {
                                alert("Error updating document: " + error);
                            });
                    });

                })

        } else {
            document.getElementById("dis_btn").checked = false;
        }
    } else if (document.getElementById("dis_btn").checked == false) {
        var res = confirm("Are you sure you want to connect this member?")
        if (res) {
            document.getElementById("dis_btn").checked = false;

            db.collection("Member-Details").where("memberNumber", "==", mem).get()
                .then(function (querySnapshot) {
                    querySnapshot.forEach(function (doc) {
                        db.collection("Member-Details").doc(doc.id).update({
                                status: "Connected",
                            })
                            .then(function () {
                                alert("Member connected successfully!");
                            })
                            .catch(function (error) {
                                alert("Error updating document: " + error);
                            });
                    });

                })
        } else {
            document.getElementById("dis_btn").checked = true;
        }
    }

}

var today = new Date();
var upd_lp;
var upd_exp;

function billing() {
    document.getElementById('bill-spinner').style.display = "block";
    document.getElementById('bill-body').style.display = "none";
    // alert('billing')
    memberNumber = document.getElementById('memN').innerText;



    db.collection("Member-Details").where("memberNumber", "==", memberNumber).get()
        .then(function (querySnapshot) {

            document.getElementById('bill-spinner').style.display = "none";
            document.getElementById('bill-body').style.display = "block";
            // alert('searched')

            querySnapshot.forEach(function (doc) {
                // alert('alright here')
                // var lastPaid = new Date(doc.data().lastPaid.toString());
                var exp = new Date(doc.data().expiry.toString());
                memDets = doc.data();

                // document.getElementById("LastDate").innerText = "Last paid on " + date.format(lastPaid, 'DD MMM, YYYY') + ".\n 
                document.getElementById("LastDate").innerText = "Water connection valid upto " + date.format(exp, 'MMM, YYYY') + ".\n \n Bill to be calculated from: ";
                billStartMonth = date.addMonths(exp, 1);
                document.getElementById("startBill").innerHTML = "<option>" + date.format(billStartMonth, 'MMM, YYYY') + "</option>";
                document.getElementById("paidUntilMonth").innerHTML = "";

                var i;
                // alert(date.subtract(exp, today).toDays());

                console.log(date.subtract(billStartMonth, today).toDays())
                var currentM = new Date(date.format(today, 'YYYY/MM'))

                if (date.subtract(billStartMonth, currentM).toDays() >= 0) {
                    console.log('Entered if')
                    for (i = 1; i < 12; i++) {
                        var nextMonth = date.addMonths(exp, i);
                        nextMonth = date.format(nextMonth, 'MMM, YYYY');
                        document.getElementById("paidUntilMonth").innerHTML += "<option>" + nextMonth + "</option>";
                    }
                    end_month = date.addMonths(exp, 1)
                    // alert("entered if")

                } else {
                    for (i = -1; i < 12; i++) {
                        var nextMonth = date.addMonths(today, i);
                        nextMonth = date.format(nextMonth, 'MMM, YYYY');
                        document.getElementById("paidUntilMonth").innerHTML += "<option>" + nextMonth + "</option>";
                    }
                    end_month = today;
                    // alert("entered else")

                }


                memberDep = doc.data().deposit;
                if (doc.data().plotType == "Residential") {
                    monthlyfee = configData["MonthRes"];
                    yearlyfee = configData["YearRes"]
                }
                if (doc.data().plotType == "Commercial") {
                    monthlyfee = configData["MonthCom"];
                    yearlyfee = configData["YearCom"]
                }

                //Check if current month is April to activate yearly option
                if(date.format(today, 'MM')=="04"){
                    document.getElementById("yearly").innerHTML = '<input type="checkbox" onchange="calcBill();" class="form-check-input" id="yearlyCheck"><label class="form-check-label" style="margin-top:7px"; for="yearlyCheck">Pay Yearly</label>'
                }

                calcBill();
                // days = parseInt(date.subtract(end_month, billStartMonth).toDays());

                // months = Math.round(days / 30) + 1;

                // ext_dep = (parseInt(depAmount) - parseInt(memberDep));

                // // Late Algorithm
                // var late = 0;
                // var late_month = billStartMonth;
                // // alert (date.subtract(today, late_month).toDays())

                // while (date.subtract(today, late_month).toDays() > 0) {
                //     console.log(date.subtract(today, late_month).toDays())

                //     //For current month only checks if day > 15 
                //     if (date.format(today, 'MM') == date.format(late_month, 'MM')) {
                //         if (parseInt(date.format(today, "DD")) > 15) {

                //             late += lateFee;
                //             late_month = date.addMonths(late_month, 1);
                //             // console.log(late_month)

                //         }
                //         break;
                //     } else {

                //         late += lateFee;
                //         late_month = date.addMonths(late_month, 1);

                //     }

                // }

                // var wat_chg = (months * monthlyfee);
                // total = ext_dep + wat_chg + late;

                // document.getElementById("calSelAmt").innerText = "Deposit Amount : " + ext_dep + "\nWater Charges : " + wat_chg + "\n Late Charges : " + late + "\n\nTotal Bill Amount : " + total.toString();

            });

        })

}

function calcBill() {
    // console.log("entered calcbill");
    var selectedM = document.getElementById('paidUntilMonth')
    var selValue = selectedM.options[selectedM.selectedIndex].text;
    selDate = selValue.slice(-4) + "," + selValue.slice(0, 3);
    var end_date = new Date(selDate);

    var start_month = new Date(billStartMonth.toString());
    var end_month = new Date(end_date.toString());

    days = parseInt(date.subtract(end_month, start_month).toDays());

    months = Math.round(days / 30) + 1;
    // console.log(monthlyfee);
    // ext_dep = (parseInt(depAmount) - parseInt(memberDep));

    // Late Algorithm
    var late = 0;
    // var late_month = billStartMonth;
    // console.log(date.subtract(date.addMonths(today, -1), late_month));

    // while (date.subtract(date.addMonths(today, -1), late_month).toDays() > 0) {

    //     //For current month only checks if day > 15 

    //     if (date.format(today, 'MM') == date.format(late_month, 'MM')) {
    //         if (parseInt(date.format(today, "DD")) > 15) {

    //             late += lateFee;
    //             late_month = date.addMonths(late_month, 1);
    //             // console.log(late_month)

    //         }
    //         break;
    //     } else {

    //         late += lateFee;
    //         late_month = date.addMonths(late_month, 1);

    //     }

    // }

    if (parseInt(date.format(today, 'DD')) < 15 && (date.format(billStartMonth, 'MM') != date.format(today, 'MM'))) {
        late = date.subtract(date.addMonths(today, -2), billStartMonth).toDays();
        late = Math.round(late / 30);
        late = late * lateFee;
    } else if (parseInt(date.format(today, 'DD')) > 15 && (date.format(billStartMonth, 'MM') != date.format(today, 'MM'))) {
        late = date.subtract(date.addMonths(today, -1), billStartMonth).toDays();
        late = Math.round(late / 30);
        late = late * lateFee;
    } else {
        late = 0;
    }
    late = late <= 0 ? 0 : late;
    //late algorithm ends

    var wat_chg;

    //yearly algorithm starts
    console.log($("#yearlyCheck").is(":checked"))
    if($("#yearlyCheck").is(":checked")){
        console.log("calculating yearly bill");
        if(date.format(billStartMonth, 'MM')!=date.format(today, 'MM')){
            wat_chg=[parseInt(date.format(today, 'MM'))-parseInt(date.format(billStartMonth, 'MM'))]*monthlyfee;
            total = wat_chg + late + parseInt(yearlyfee);
            console.log(wat_chg)
            document.getElementById("calSelAmt").innerText = "Due Water Charges for current year: " + wat_chg + "\n Late Charges : " + late + "\n Yearly Charges : " + yearlyfee + "\n\nTotal Bill Amount : " + total;
        }
        else if(date.format(billStartMonth, 'MM')!=date.format(today, 'MM')){
            wat_chg=0;
            total = wat_chg + late + parseInt(yearlyfee);
            console.log(wat_chg)
            document.getElementById("calSelAmt").innerText = "Due Water Charges for current year: " + wat_chg + "\n Late Charges : " + late + "\n Yearly Charges : " + yearlyfee + "\n\nTotal Bill Amount : " + total;
        
        }

    }
    else{
        console.log("Not calculating yearly bill");
        wat_chg = (months * monthlyfee);
        total = wat_chg + late
        console.log(wat_chg)
        document.getElementById("calSelAmt").innerText = "Water Charges: " + wat_chg + "\n Late Charges : " + late + "\n\nTotal Bill Amount : " + total;

    }
    //yearly algorithm ends
    
    
    // document.getElementById("calSelAmt").innerText = "Water Charges : " + wat_chg + "\n Late Charges : " + late + "\n\nTotal Bill Amount : " + total;

    //Bill ID generation 
    var rnd = Math.random().toString(36).substring(10)
    bill_id = date.format(today, 'YYMMDD') + memDets["memberNumber"] + rnd
    console.log(typeof (bill_id))

    bill_info = {
        "memberNumber": memDets["memberNumber"],
        "paidOn": date.format(today, 'YYYY/M/DD'),
        "paidUntil": date.format(end_month, 'YYYY/MMM'),
        "total": total,
        "bID": bill_id
    }

    queryString = '?' + date.format(today, 'DD/MM/YYYY') + "&" + memDets["memberNumber"] + "&" + memDets["fullName"] + "&" + memDets["phone"] + "&" + wat_chg + "&" + late + "&" + date.format(start_month, 'MMM YYYY') + '&' + date.format(end_month, 'MMM YYYY') + '&' + bill_id;
    // alert(typeof(date.format(today,'DD/MM/YYYY')))
}


function updModal() {
    document.getElementById("upd-plot").placeholder = memDets["plot"];
    document.getElementById("upd-memberNumber").placeholder = memDets["memberNumber"];
    document.getElementById("upd-fullName").placeholder = memDets["fullName"];
    document.getElementById("upd-phone").value = memDets["phone"];
    document.getElementById("upd-selectedPlotType").value = memDets["plotType"];

    document.getElementById("upd-plot").disabled = true;
    document.getElementById("upd-memberNumber").disabled = true;
    document.getElementById("upd-fullName").disabled = true;

}

function updateMember() {

    var mem = memDets["memberNumber"];
    var sel = document.getElementById('upd-selectedPlotType')
    var selValue = sel.options[sel.selectedIndex].text;
    console.log(memDets["memberNumber"] + selValue + document.getElementById("upd-phone").value)

    // Set the "capital" field of the city 'DC'

    db.collection("Member-Details").where("memberNumber", "==", mem).get()
        .then(function (querySnapshot) {
            querySnapshot.forEach(function (doc) {
                db.collection("Member-Details").doc(doc.id).update({
                        plotType: selValue,
                        phone: document.getElementById("upd-phone").value.toString()
                    })
                    .then(function () {
                        alert("Member successfully updated!");
                        $('#updateModal').modal('hide');
                        window.location.reload();
                    })
                    .catch(function (error) {
                        // The document probably doesn't exist.
                        alert("Error updating document: " + error);
                    });
            });

        })
}

function genBill() {
    console.log(queryString);

    // console.log(bill_info)

    // Creating a new bill
    db.collection("Bills-Generated").doc().set(bill_info)
        .then(function () {
            // alert("Bill generated successfully!");
            window.open("bill.html" + queryString);

            // window.location.reload()
        })
        .catch(function (error) {
            alert("An error occured. Please retry.");
        });

    // Updating Last paid and expiry
    db.collection("Member-Details").where("memberNumber", "==", memDets["memberNumber"]).get()
        .then(function (querySnapshot) {
            querySnapshot.forEach(function (doc) {
                db.collection("Member-Details").doc(doc.id).update({
                        lastPaid: selValue,
                        expiry: document.getElementById("upd-phone").value.toString()
                    })
                    .then(function () {
                        alert("Member successfully updated!");
                        $('#updateModal').modal('hide');
                        window.location.reload();
                    })
                    .catch(function (error) {
                        alert("Error updating document: " + error);
                    });
            });

        })


}

function transferOwner() {
    var newname = document.getElementById("new-name").value;
    var newnumber = document.getElementById("new-phone").value;
    var ts = document.getElementById('newSelectedPlotType')
    var newType = ts.options[ts.selectedIndex].text;

    var transJSON = {
        "dateChanged": date.format(today, "YYYY/MM/DD"),
        "memberNumber": memDets["memberNumber"],
        "newData": [newname, newType, newnumber],
        "oldData": [memDets["fullName"], memDets["plotType"], memDets["phone"]],

    }

    db.collection("Ownership-Transfers").doc().set(transJSON)
        .then(function () {
            alert("Ownership transferred successfully!");
            window.location.reload()
        })
        .catch(function (error) {
            alert("An error occured. Please retry.");
        });

    db.collection("Member-Details").where("memberNumber", "==", memDets["memberNumber"]).get()
        .then(function (querySnapshot) {
            querySnapshot.forEach(function (doc) {
                db.collection("Member-Details").doc(doc.id).update({
                        fullName: newname,
                        plotType: newnumber,
                        phone: newType,
                    })
                    .then(function () {
                        // alert("Member successfully updated!");
                        $('#updateModal').modal('hide');
                        window.location.reload();
                    })
                    .catch(function (error) {
                        // The document probably doesn't exist.
                        alert("An error occured. ");
                    });
            });

        })
}

function checkTransfer() {
    memString = "?" + memDets["memberNumber"];
    window.open("ownerHistory.html" + memString);
}

function deleteMember() {
    res = confirm("If you proceed, this Member's data will be completely lost. Are you sure you want to proceed anyway?");
    if (res == true) {
        var mem = memDets["memberNumber"];

        db.collection("Member-Details").where("memberNumber", "==", mem).get()
            .then(function (querySnapshot) {
                querySnapshot.forEach(function (doc) {

                    result = confirm("Are you sure you want to permanently delete this member?");

                    if (result == true) {

                        db.collection("Member-Details").doc(doc.id).delete()
                            .then(function () {
                                alert("Member successfully deleted!");
                                window.location.reload();

                            })
                            .catch(function (error) {
                                // The document probably doesn't exist.
                                alert("Error deleting the member. Please retry. \n" + error);
                                window.location.reload();

                            });

                    }
                });

            })

    }
}

function toggle_search() {

    if (document.getElementById("s-tog").checked==true) {
        document.getElementById("search_input").style.display = "inline-block";
        document.getElementById("mem_s_btn").style.display = "inline-block";

        document.getElementById("bill_search").style.display = "none";
        document.getElementById("bill_btn").style.display = "none";
        // document.getElementById("s-tog").checked = false;

    } 
    else if (document.getElementById("s-tog").checked==false){
        document.getElementById("search_input").style.display = "none";
        document.getElementById("mem_s_btn").style.display = "none";

        document.getElementById("bill_search").style.display = "inline-block";
        document.getElementById("bill_btn").style.display = "inline-block";

        // document.getElementById("s-tog").checked = true;

    }
}