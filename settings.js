const storage = require('electron-json-storage');
const firebase = require("firebase");
const date = require('date-and-time');

require("firebase/firestore");

firebase.initializeApp({
    apiKey: "AIzaSyDRAurj89rKfwwBMoRjQ5JfzRKUDM9ax-I",
    authDomain: "desktop-app-8b038.firebaseapp.com",
    projectId: "desktop-app-8b038",
});

var db = firebase.firestore();

let configData;

storage.get('config', function (error, tdata) {
    if (error) {
        console.log("Error: " + error)
    } else {
        console.log(tdata);
        configData = tdata;
        fillCurrentData();
    }

});

storage.get('admin_stat', function (error, status) {
    if (error) {
        console.log("Error: " + error)
    } else {
        a_data = status;
        admin_status = parseInt(a_data["admin"]);
        console.log(admin_status)

        if (Boolean(admin_status) == true) {
            // alert('entered ' + Boolean(admin_status))
            var arrayOfElements = document.getElementsByClassName('admin_feat');
            var lengthOfArray = arrayOfElements.length;

            for (var i = 0; i < lengthOfArray; i++) {
                arrayOfElements[i].style.display = 'block';
            }
        }
    }
});

function fillCurrentData() {
    console.log(configData);
    document.getElementById("monR").value = configData["MonthRes"];
    document.getElementById("yearR").value = configData["YearRes"];
    document.getElementById("monC").value = configData["MonthCom"];
    document.getElementById("yearC").value = configData["YearCom"];
    document.getElementById("lateF").value = configData["MonLateFee"];
    document.getElementById("instC").value = configData["InstCharges"];
    document.getElementById("depA").value = configData["DepAmt"];
}


function setNewvalues() {
    document.getElementById("monR").disabled = false;
    document.getElementById("yearR").disabled = false;
    document.getElementById("monC").disabled = false;
    document.getElementById("yearC").disabled = false;
    document.getElementById("lateF").disabled = false;
    document.getElementById("instC").disabled = false;

    document.getElementById("depA").disabled = false;
    document.getElementById("monR").focus();
    document.getElementById("change").style.display = "none";
    document.getElementById("set").style.display = "block";
    var inp = document.getElementsByClassName("dis");
    for (var i = 0; i < inp.length; i++) {
        inp[i].style.backgroundColor = "white";
        inp[i].style.cursor = "pointer";
    }

}

function confirmChanges() {
    mRes = document.getElementById("monR").value;
    yRes = document.getElementById("yearR").value;
    mCom = document.getElementById("monC").value;
    yCom = document.getElementById("yearC").value;
    lateF = document.getElementById("lateF").value;
    instChar = document.getElementById("instC").value;

    depA = document.getElementById("depA").value;

    document.getElementById("load-spinner").style.display="inline-block";

    new_setting = {
        DepAmt: depA,
        InstCharges: instChar,
        MonLateFee: lateF,
        MonthCom: mCom,
        MonthRes: mRes,
        YearCom: yRes,
        YearRes: yCom
    };
    db.collection("Configuration").get()
        .then(function (querySnapshot) {
            querySnapshot.forEach(function (doc) {
                    console.log(doc.data())
                    db.collection("Configuration").doc(doc.id).update(new_setting)
                        .then(function () {
                            document.getElementById("load-spinner").style.display="none";
                            alert("Settings successfully updated! Please restart the application to update the changes made.");
                            window.close();
                        })
                        .catch(function (error) {
                            document.getElementById("load-spinner").style.display="none";
                            // The document probably doesn't exist.
                            alert("Error updating document: " + error);
                        });


                }

            )
        })
}